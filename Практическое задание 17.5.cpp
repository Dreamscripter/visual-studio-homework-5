﻿// Практическое задание 17.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>

class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }
    double length()
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }
};

int main()
{
    Vector test(6, 6, 7);
    test.show();
    std::cout << test.length() << '\n';
}

